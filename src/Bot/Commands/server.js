const axios = require("axios");
var pretty = require("prettysize");
const Discord = require("discord.js");

exports.run = async (client, message, args) => {
  const otherargs = message.content
    .split(" ")
    .slice(3)
    .join(" ");
    
          const offered = new Discord.MessageEmbed()
        .setColor(client.Color)
        .setTitle("Astronomy Development - Server Creation List")
        .setDescription("Hey babe! Here are some servers we provide <3")
        .addField(
          "Bots",
          `nodejs - non custom startup command`
        )
        .addField("Minecraft", `forge \n paper `)
        .addField("Games", `gmod \n cs:go \n ark:se`, true)
        .addField("Databases", `redis \n mongodb`)

  const needName = new Discord.MessageEmbed()
    .setTitle("Astrolgy Development - Server Creation Error")
    .setColor(client.Color)
    .setDescription(
      "Hey! You forgot to give a name for your sever :/ \n try this command again but add a cool name for your server <3 \n Example: `AH!server create type cool-server` "
    );

  const failedEmbed = new Discord.MessageEmbed()
    .setTitle("Astrolgy Development - Server Creation Error")
    .setColor(client.Color)
    .setDescription(
      "Hey! We ran into an error while trying to create you a server, \n you can either wait a few seconds and try again or you could open up a ticket so we can find out what the issue is. \n Ticket Command: `AH!ticket new`  "
    );

  if (userData.get(message.author.id) == null) {
    message.channel.send(
      "Oh no, Seems like you do not have an account linked to your discord ID. \nIf you have not made an account yet please check out `" +
        "AH!" +
        "user new` to create an account \nIf you already have an account link it using `" +
        "AH!" +
        "user link`"
    );
  }

  if (!args[0]) {
    const firstEmbed = new Discord.MessageEmbed()
      .setTitle("Astrolgy Development - Help")
      .setColor(client.Color)
      .setDescription(
        "Need to create a server? Edit a server? Connect a domain? \n Well you found the right command!"
      )
      .addField(
        "User Commands",
        ` \`AH!server info serverID\` - Gets you some cool facts about your server. \n \`AH!server create type name\` - Gives you a list of servers you can create. \n \`AH!server delete serverID\` - Delete a server you own. \n \`AH!server proxy DOMAIN serverID\` - Connect your server to your domain. \n \`AH!server types\` - Get the types of servers we offer
`
      );
    return message.channel.send(firstEmbed);
  }
    
   if(args[0].toLowerCase() === "types") {
       return message.channel.send(offered);
   }

  if (args[0].toLowerCase() === "info") {
    if (!args[1]) {
      let secEmbed = new Discord.MessageEmbed()
        .setColor(client.ErrorColor)
        .setDescription(
          "Please specify a server. Example: `AH!server info serverID`"
        );
      return message.channel.send(secEmbed);
    }

    let awaiterf = await message.channel.send(
      "Getting server information please wait..."
    );

    axios({
      url: config.Pterodactyl.hosturl + "/api/client/servers/" + args[1],
      method: "GET",
      followRedirect: true,
      maxRedirects: 5,
      headers: {
        Authorization: "Bearer " + config.Pterodactyl.apikeyclient,
        "Content-Type": "application/json",
        Accept: "Application/vnd.pterodactyl.v1+json"
      }
    }).then(response => {
      axios({
        url:
          config.Pterodactyl.hosturl +
          "/api/client/servers/" +
          args[1] +
          "/resources",
        method: "GET",
        followRedirect: true,
        maxRedirects: 5,
        headers: {
          Authorization: "Bearer " + config.Pterodactyl.apikeyclient,
          "Content-Type": "application/json",
          Accept: "Application/vnd.pterodactyl.v1+json"
        }
      }).then(resources => {
        let color = client.Color;

        if (resources.data.attributes.current_state === "starting") {
          color = "GREEN";
        }

        if (resources.data.attributes.current_state === "offline") {
          color = client.ErrorColor;
        }

        let embedstatus = new Discord.MessageEmbed()
          .setColor(color)
          .addField("**Status**", resources.data.attributes.current_state, true)
          .addField(
            "**CPU Usage**",
            resources.data.attributes.resources.cpu_absolute + "%"
          )
          .addField(
            "**RAM Usage**",
            pretty(resources.data.attributes.resources.memory_bytes) +
              "  out of UNLIMITED MB"
          )
          .addField(
            "**DISK Usage**",
            pretty(resources.data.attributes.resources.disk_bytes) +
              "  out of UNLIMITED MB"
          )
          .addField(
            "**NET Usage**",
            "UPLOADED: " +
              pretty(resources.data.attributes.resources.network_tx_bytes) +
              ", DOWNLOADED: " +
              pretty(resources.data.attributes.resources.network_rx_bytes)
          )
          .addField("**NODE**", response.data.attributes.node)
          .addField("**FULL ID**", response.data.attributes.uuid)
          .addField("\u200b", "\u200b")
          .addField(
            "**LIMITS (0 = unlimited)**",
            "MEMORY: " +
              response.data.attributes.limits.memory +
              "MB \nDISK: " +
              response.data.attributes.limits.disk +
              "MB \nCPU: " +
              response.data.attributes.limits.cpu
          )
          .addField(
            "Other",
            "DATABASES: " +
              response.data.attributes.feature_limits.databases +
              "\nBACKUPS: " +
              response.data.attributes.feature_limits.backups
          );
        awaiterf.delete();
        message.channel.send(embedstatus);
      });
    });
  }

  if (args[0].toLowerCase() === "create") {
    let info = client.userAllowed.get(message.author.id);
    let has;
    let allowed;
    if (!info) {
      let data = {
        id: message.author.id,
        desc: null,
        allowed: 1,
        has: 0,
        payments: []
      };

      client.userAllowed.set(message.author.id, data);

      has = 0;
      allowed = 1;
    } else {
      has = info.has;
      allowed = info.allowed;
    }

    if (has >= allowed) {
      let thEmbed = new Discord.MessageEmbed()
        .setColor(client.ErrorColor)
        .setTitle("Astronomy Development - Server Creation Error")
        .setDescription(
          "I ran into an issue while trying to process this command for you, error: \n " +
            `You can not create anymore servers, please open a ticket and ask how to get more servers! \n Info: Ticket Command: \`AH!ticket new\` \n Has: \`${has}\` server(s) \n Allowed: \`${allowed}\` server(s) `
        );

      return message.channel.send(thEmbed);
    }

    if (!args[1]) {
      let secEmbed = new Discord.MessageEmbed()
        .setColor(client.ErrorColor)
        .setTitle("Astronomy Development - Server Creation")
        .setDescription(
          "Please specify a server type. \n For the types of servers we offer do: `AH!server create list` \n **FYI:** \n Server Lmits for you: `" +
            allowed +
            "` \n You have: `" +
            has +
            "` server(s)"
        );
      return message.channel.send(secEmbed);
    }

    if (args[1].toLowerCase() === "list") {
      return message.channel.send(offered);
    }

    if (args[1].toLowerCase() === "nodejs") {
      if (!otherargs) {
        return message.channel.send(needName);
      }


               //Data to send
                    const data = {
                        "name": otherargs,
                        "user": userData.get(message.author.id + ".consoleID"),
                        'description': 'panel administrator to fill in',
                        "pack": 6, // or nest
                        "egg": 17,
                        "docker_image": "quay.io/parkervcp/pterodactyl-images:debian_nodejs-12",
                        "startup": `if [[ -d .git ]] && [[ {{AUTO_UPDATE}} == "1" ]]; then git pull; fi && /usr/local/bin/npm install --production && /usr/local/bin/node /home/container/{{BOT_JS_FILE}}`,
                        "limits": {
                            "memory": 0,
                            "swap": 0,
                            "disk": 200,
                            "io": 500,
                            "cpu": 0
                        },
                        "environment": {
                            "INSTALL_REPO": null,
                            "INSTALL_BRANCH": null,
                            "USER_UPLOAD": "0",
                            "AUTO_UPDATE": "0",
                            "BOT_JS_FILE": "index.js"
                        },
                        "feature_limits": {
                            "databases": 0,
                            "allocations": 1,
                            "backups": 10
                        },
                        "deploy": {
                            "locations": [1],
                            "dedicated_ip": false,
                            "port_range": []
                        },
                        "start_on_completion": false
                    };

                    //Sending the data:
                    axios({
                        url: config.Pterodactyl.hosturl + "/api/application/servers",
                        method: 'POST',
                        followRedirect: true,
                        maxRedirects: 5,
                        headers: {
                            'Authorization': 'Bearer ' + config.Pterodactyl.apikey,
                            'Content-Type': 'application/json',
                            'Accept': 'Application/vnd.pterodactyl.v1+json',
                        },
                        data: data,
                    })
        .then(response => {
                        console.log(response)
          let embed = new Discord.MessageEmbed()
            .setColor(client.Color)
            .addField(`Server Status: `, response.statusText)
            .addField(`Created for who?: `, data.user)
            .addField(`Server name: `, data.name)
            .addField(`Server Type: `, args[1].toLowerCase());
          message.channel.send(embed);
        })
        .catch(error => {
          let messageEmbed1 = new Discord.MessageEmbed()
            .setTitle("Astrolgy Development - Server Creation Error")
            .setColor(client.Color)
            .setDescription(
              `I was trying to setup a nodejs server for: ${message.author.tag} \n ${error}`
            );
          client.channels.cache.get("770352440788647955").send(messageEmbed1);
          message.channel.send(failedEmbed);
        });
    } else if (args[1].toLowerCase() === "sinusbot") {
      if (!otherargs) {
        return message.channel.send(needName);
      }
    } else if (args[1].toLowerCase() === "forge") {
      if (!otherargs) {
        return message.channel.send(needName);
      }
    } else if (args[1].toLowerCase() === "paper") {
      if (!otherargs) {
        return message.channel.send(needName);
      }
    } else if (args[1].toLowerCase() === "gmod") {
      if (!otherargs) {
        return message.channel.send(needName);
      }
    } else if (args[1].toLowerCase() === "cs:go") {
      if (!otherargs) {
        return message.channel.send(needName);
      }
    } else if (args[1].toLowerCase() === "ark:se") {
      if (!otherargs) {
        return message.channel.send(needName);
      }
    } else {
      return message.channel.send(offered);
    }
  }
    
        if(args[0].toLowerCase() === "proxy") {

					const embed = new Discord.MessageEmbed()
            .setTitle('__**How to link a domain to a website/server**__ \nCommand format: ' + "AH!" + 'server proxy domainhere serverid')
        if (!args[1]) {
            message.channel.send(embed)
        } else {
            if (!args[2]) {
                message.channel.send(embed)
            } else {
                message.channel.send('Please give me a few seconds. Trying to link that domain!')
                //SSH Connection
                ssh.connect({
                    host: config.SSH.Host,
                    username: config.SSH.User,
                    port: config.SSH.Port,
                    password: config.SSH.Password,
                    tryKeyboard: true,
                })

                //Copy template file. Ready to be changed!
                fs.access(path.resolve(path.dirname(require.main.filename), "proxy/" + args[1].toLowerCase() + ".conf"), fs.constants.R_OK, (err) => {
                    if (!err) {
                        return message.channel.send("This domain has been linked before or is currently linked..")
                    } else {
                        fs.copyFile(path.resolve('./proxy/template.txt'), './proxy/' + args[1] + '.conf', (err) => {
                            if (err) {
                                console.log("Error Found:", err);
                            }
                        })
                        fs.copyFile(path.resolve('./proxy/template.txt'), './proxy/' + args[1] + '.conf', (err) => {
                            if (err) {
                                console.log("Error Found:", err);
                            }
                        })

                        setTimeout(() => {
                            //Change Domain
                            var z = 0;
                            while (z < 5) {
                                const domainchange = rif.sync({
                                    files: '<proxy-file-location>' + args[1] + '.conf',
                                    from: "REPLACE-DOMAIN",
                                    to: args[1].toLowerCase(),
                                    countMatches: true,
                                });
                                z++
                            }

                            //Grab node and port ready for the config 
                            axios({
                                url: config.Pterodactyl.hosturl + "/api/client/servers/" + args[2],
                                method: 'GET',
                                followRedirect: true,
                                maxRedirects: 5,
                                headers: {
                                    'Authorization': 'Bearer ' + config.Pterodactyl.apikeyclient,
                                    'Content-Type': 'application/json',
                                    'Accept': 'Application/vnd.pterodactyl.v1+json',
                                }
                            }).then(response => {
                                const node = response.data.attributes.node;
                                console.log(node)
                                const port = response.data.attributes.relationships.allocations.data[0].attributes.port
                                if (node === "Node 1") {

                                    //Change Server IP
                                    setTimeout(() => {
                                        var y = 0;
                                        while (y < 3) {
                                            const ipchange = rif.sync({
                                                files: '<root-proxy-file-location>' + args[1] + '.conf',
                                                from: "REPLACE-IP",
                                                to: "161.97.126.45",
                                                countMatches: true,
                                            });
                                            y++
                                        };

                                        //Change Server Port
                                        setTimeout(() => {
                                            var x = 0;
                                            while (x < 3) {
                                                const portchange = rif.sync({
                                                    files: '<root-proxy-file-location>' + args[1] + '.conf',
                                                    from: "REPLACE-PORT",
                                                    to: port,
                                                    countMatches: true,
                                                });
                                                x++
                                            }
                                        }, 100) //END - Change Server Port
                                    }, 100) //END - Change Server IP
                                } else if (node === "Node 2") {

                                    //Change Server IP
                                    setTimeout(() => {
                                        var y = 0;
                                        while (y < 3) {
                                            const ipchange = rif.sync({
                                                files: '<root-proxy-file-location>' + args[1] + '.conf',
                                                from: "REPLACE-IP",
                                                to: "161.97.126.45",
                                                countMatches: true,
                                            });
                                            y++
                                        };

                                        //Change Server Port
                                        setTimeout(() => {
                                            var x = 0;
                                            while (x < 3) {
                                                const portchange = rif.sync({
                                                    files: '<root-file-proxy-location>' + args[1] + '.conf',
                                                    from: "REPLACE-PORT",
                                                    to: port,
                                                    countMatches: true,
                                                });
                                                x++
                                            }
                                        }, 100) //END - Change Server Port
                                    }, 100) //END - Change Server IP

 
                                } else {
                                    message.channel.send('Unsupported node. Stopping reverse proxy.')
                                    fs.unlinkSync("./proxy/" + args[1] + ".conf");
                                }


                                //Upload file to /etc/apache2/sites-available
                                setTimeout(() => {
                                    ssh.putFile('<root-file-proxy-location>' + args[1] + '.conf', '/etc/apache2/sites-available/' + args[1] + ".conf").then(function () {

                                        //Run command to genate SSL cert.
                                        ssh.execCommand(`certbot certonly -d ${args[1]} --non-interactive --webroot --webroot-path /var/www/html --agree-tos -m <email>`, {
                                            cwd: '/root'
                                        }).then(function (result) {
                                            if (result.stdout.includes('Congratulations!')) {
                                                //No error. Continue to enable site on apache2 then restart
                                                console.log('SSL Gen complete. Continue!')

                                                ssh.execCommand(`a2ensite ${args[1]} && service apache2 restart`, {
                                                    cwd: '/root'
                                                }).then(function (result) {
                                                    //Complete
                                                    message.reply('Domain has now been linked!')

                                                    /*
                                                    domains.set(args[1], {
                                                        DiscordID: message.author.id,
                                                        ServerID: args[2],
                                                        Domain: args[1]
                                                      });
                                                    */
                                                })
                                            } else if (result.stdout.includes('Certificate not yet due for renewal')) {
                                                //No error. Continue to enable site on apache2 then restart
                                                console.log('SSL Gen complete. Continue!')

                                                ssh.execCommand(`a2ensite ${args[1]} && service apache2 restart`, {
                                                    cwd: '/root'
                                                }).then(function (result) {
                                                    //Complete
                                                    message.reply('Domain has now been linked!')

                                                    /*
                                                    domains.set(args[1], {
                                                        DiscordID: message.author.id,
                                                        ServerID: args[2],
                                                        Domain: args[1]
                                                      });
                                                    */
                                                })
                                            } else {
                                                message.channel.send('Error making SSL cert. Either the domain is not pointing to `154.27.68.234` or cloudflare proxy is enabled!\n\n' +
                                                    '**If you have just done this after running the command. Please give the bot 5 - 10mins to refresh the DNS cache** \n\nFull Error: ```' + result.stdout + '```')
                                                fs.unlinkSync("./proxy/" + args[1] + ".conf");
                                            }
                                        })
                                    }, function (error) {
                                        //If error exists. Error and delete proxy file
                                        fs.unlinkSync("./proxy/" + args[1] + ".conf");
                                        message.channel.send("FAILED \n" + error);
                                    })
                                }, 250) //END - Upload file to /etc/apache2/sites-available
                            }).catch(err => {
                                message.channel.send('Can\'t find that server :( ')
                                fs.unlinkSync("./proxy/" + args[1] + ".conf");
                            }) //END - Grab server info (Node and Port)
                        }, 250) //END - //Change Domain
                    }
                })
            }
        
				}
    
};

exports.help = {
  name: "server",
  description: "mutes or unmutes a mentioned user",
  usage: "mute [mention] [reason]"
};

exports.conf = {
  Aliases: []
};
