const Discord = require("discord.js");

exports.run = async (Mythical, message, args) => {
  if (!Mythical.Developers.includes(message.author.id)) {
    return undefined;
  }

  if (!args[0]) {
    return message.channel
      .send("Please provide something to say.")
      .then(msg => msg.delete({ timeout: 5000 }));
  }

  const arg = message.content
    .split(" ")
    .slice(1)
    .join(" ");
  message.delete();
  message.channel.send(arg);
};

exports.help = {
  name: "say",
  description: "Evaluate some code in javascript.",
  usage: "m!Eval <Value>"
};

exports.conf = {
  Aliases: []
};