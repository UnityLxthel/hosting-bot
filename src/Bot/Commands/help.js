const Discord = require("discord.js");

module.exports.run = async (Mythical, message, args) => {
  // eslint-disable-line no-unused-vars
  const emebed = new Discord.MessageEmbed()
    .setTitle("Astrolgy Development - Help")
    .setColor(Mythical.Color)
    .addField(
      "User Commands:",
      `
\`AH!user\` - Gives you options for your account. 
\`AH!server\` - **not done** Gives you options on server creation and editing.
`
    );

  if (Mythical.Staff.includes(message.author.id)) {
    emebed.addField(
      "Staff Commands",
      `
\`AH!purge\` - you know, gets rid of messages or something like that....
`
    );
  }

  if (Mythical.Developers.includes(message.author.id)) {
    emebed.addField(
      "Panel Admin",
      `
\`AH!eval\` 
`
    );
  }

  return message.channel.send(emebed);
};

exports.help = {
  name: "help",
  description: "Allows you to test the welcome!",
  usage: "AR!Join"
};

exports.conf = {
  Aliases: ["h"]
};
