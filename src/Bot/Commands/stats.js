exports.run = async(client, message) => {
    
  //Sends message while the json's are being cached
  const Discord = require("discord.js")
  let embed = new Discord.MessageEmbed()
  .setColor(`RANDOM`)
  .addField(`__**Please wait...**__`, `Loading all node's stats! (If this takes longer than 5seconds. This command is broken)`, true);
  let msg = await message.channel.send(embed)

  //Edits the message to display the data
      embed.fields.pop()
          embed.addField("\u200b", "__**[Node 1 - Bots, Websites and Databases](https://danbot.host/Node1)**__ \n**CPU LOAD**: " + nodeData.fetch("primary-node.cpuload") + "% \n**RAM (USED/TOTAL)**: " + nodeData.fetch("primary-node.memused") + " / " + nodeData.fetch("primary-node.memtotal") + " \n**STORAGE (USED/TOTAL)**: " + nodeData.fetch("primary-node.diskused") + " / " + nodeData.fetch("primary-node.disktotal") + " \n**UPTIME**: " + nodeData.fetch("primary-node.osuptime") + "\n**Servers:** **Total**: " + nodeData.fetch("primary-node.dockercontainers") + ", **Running**: " + nodeData.fetch("primary-node.dockercontainersrunning") + ", **Stopped**: " + nodeData.fetch("primary-node.dockercontainersstopped"))
          embed.addField('\u200b', '\u200b')
          embed.addField("\u200b", "__**[Node 2 - Bots, Websites and Databases](https://danbot.host/Node2)**__ \n**CPU LOAD**: " + nodeData.fetch("sec-node.cpuload") + "% \n**RAM (USED/TOTAL)**: " + nodeData.fetch("sec-node.memused") + " / " + nodeData.fetch("sec-node.memtotal") + " \n**STORAGE (USED/TOTAL)**: " + nodeData.fetch("sec-node.diskused") + " / " + nodeData.fetch("sec-node.disktotal") + " \n**UPTIME**: " + nodeData.fetch("sec-node.osuptime") + "\n**Servers**: **Total**: " + nodeData.fetch("sec-node.dockercontainers") + ", **Running**: " + nodeData.fetch("sec-node.dockercontainersrunning") + ", **Stopped**: " + nodeData.fetch("sec-node.dockercontainersstopped"))
          embed.addField('\u200b', '\u200b')
          embed.setDescription('Want to view more stats live? [Click Here!](https://danbot.host/stats)')
      msg.edit(embed);
};
exports.help = {
  name: "stats",
  description: "Evaluate some code in javascript.",
  usage: "m!stats"
};

exports.conf = {
  Aliases: []
};