const Discord = require("discord.js");
const exec = require('child_process').exec;

module.exports.run = async (Koala, message, args) => {
  // eslint-disable-line no-unused-vars
  const deniedEmbed = new Discord.MessageEmbed()
    .setTitle("Astrolgy - Hub")
    .setColor(Koala.Color)
    .setDescription("Sorry, you don't have permission to use this..");

  if (!Koala.Developers.includes(message.author.id)) {
    return message.channel.send(deniedEmbed);
  }
    
    if(!args[0])return message.channel.send("Send me something to execute pls")

    const start = process.hrtime();
    exec(`${args.join(" ")}`, (error, stdout) => {
      let response = (error || stdout);
      if (response.length > 1024) console.log(response), response = 'Output too long.';
      const end = process.hrtime(start);
        
        let embed = new Discord.MessageEmbed()
        .setColor(Koala.Color)
        .setTitle("Astronomy Development - Executed")
        .setDescription(`${response}`)
        message.channel.send(embed)
    })

};

exports.help = {
  name: "execute",
  description: "Allows you to test the welcome!",
  usage: "k!Join"
};

exports.conf = {
  Aliases: ["ex", "e"]
};