const Discord = require("discord.js");

let permsToHave = [
  "VIEW_CHANNEL",
  "SEND_MESSAGES",
  "ATTACH_FILES",
  "READ_MESSAGE_HISTORY",
  "ADD_REACTIONS"
];

module.exports.run = async (Mythical, message, args) => {
  const arg = message.content
    .split(" ")
    .slice(1)
    .join(" ");
  const user = message.content
    .split(" ")
    .slice(2)
    .join(" ");

  if (!args[0]) {
    let embed = new Discord.MessageEmbed()
      .setColor(Mythical.Color)
      .setTitle("Astronomy Development - Ticket System")
      .setDescription(
        `To open a ticket: \`${config.Prefix}ticket new\` \n To Upgrade a ticket: \`${config.Prefix}ticket upgrade\` \n To Close a ticket: \`${config.Prefix}ticket close\` \n To see your past tickets: \`${config.Prefix}ticket logs\` `
      );
    return message.channel.send(embed);
  }

  if (args[0].toLowerCase() === "new") {
    let channel = await message.guild.channels.create(
      message.author.username + "-Ticket",
      {
        permissionOverwrites: [
          {
            deny: "VIEW_CHANNEL",
            id: "770348084395638795"
          },
          {
            allow: permsToHave,
            id: message.author.id
          },
          {
            allow: permsToHave,
            id: "770353655164829748" // admins
          },
          {
            allow: permsToHave,
            id: "770436026010959883" // server support
          },
          {
            allow: permsToHave,
            id: "770354006265823313" // server mods
          }
        ],
        parent: "772113945909526528",
        reason: "ticket",
        topic: `**ID:** ${message.author.id} -- **Tag:** ${message.author.tag} | Ticket`
      }
    );

    message.reply(`Please check <#${channel.id}> to view your ticket.`);

    let embedToSend = new Discord.MessageEmbed()
      .setColor(Mythical.Color)
      .setTitle("Astronomy Ticket System - " + message.author.username)
      .setDescription(
        "Here is your ticket! Please give as much info as possible about your problem."
      );

    if (userData.get(message.author.id) == null) {
      embedToSend.setFooter("This Account is not linked with our panel");
      channel.send(embedToSend).then(() => {
        channel.send(`<@${message.author.id}>`);
      });
    } else {
      embedToSend.setFooter("This Account is linked with our panel");
      channel.send(embedToSend).then(() => {
        channel.send(`<@${message.author.id}>`);
      });
    }
  }

  if (args[0].toLowerCase() === "close") {
    if (message.channel.name.includes("-ticket")) {
      const filter2 = m => m.author.id === message.author.id;
      const warning = await message.channel.send(
        "<@" +
          message.author.id +
          "> are you sure you want to close this ticket? please type `confirm` to close the ticket or `cancel` to keep the ticket open."
      );

      let collected1 = await message.channel
        .awaitMessages(filter2, {
          max: 1,
          time: 30000,
          errors: ["time"]
        })
        .catch(x => {
          warning.delete();
          message.channel.send(
            `ERROR: User failed to provide an answer. Ticket staying open.`
          );
          setTimeout(() => {
            message.channel.delete();
          }, 3000);
          return false;
        });

      if (collected1.first().content === "confirm") {
        return message.channel
          .send("**Closing ticket. Please Wait**", null)
          .then(
            setTimeout(() => {
              message.channel.delete();
            }, 5000)
          );
      } else if (collected1.first().content === "cancel") {
        return message.channel.send("Ticket Closing Was Canceled");
      }
    } else if (!message.channel.name.includes("-ticket")) {
      message.channel.send("You can only use this command in ticket channels.");
    }
  }
};

exports.help = {
  name: "ticket",
  description: "Allows you to test the welcome!",
  usage: "AR!Join"
};

exports.conf = {
  Aliases: []
};
