// gets a nodes status by fetching a servers info

const axios = require("axios");
var ping = require("ping");
const Discord = require("discord.js");

module.exports = async Astro => {
  console.log("Astronomy Hosting (Bot) Fetching nodes status.");

  //Node status
  Astro.channels.cache
    .get("770350945971470377")
    .messages.fetch("771547100739141653")
    .then(msg => {
      const embed = new Discord.MessageEmbed()

        .setTitle("Astronomy Development - Status")
        .addField(
          "System Status:",
          `**Primary Node Is: ${
            nodeStatus.get("node1").status || "<:dnd:772140858903625759> Failed to get stats"
          }** \n **Secondary Node Is: ${nodeStatus.get("node2").status || "<:dnd:772140858903625759> Failed to get stats"}**`
        )
        .addField(
          "Service Status:",
          `**Panel Website Is:** **${
            nodeStatus.get("http://161.97.126.45").status || "<:dnd:772140858903625759> Failed to get stats"
          }** \n **CDN Is** **${
            nodeStatus.get("https://cdn.astrobot.org").status || "<:dnd:772140858903625759> Failed to get stats"
          }**`
        )
        .setColor(Astro.Color);
      msg.edit(embed);
    });

  axios({
    url: config.Pterodactyl.hosturl + "/api/client/servers/fff176bc/resources",
    method: "GET",
    followRedirect: true,
    maxRedirects: 5,
    headers: {
      Authorization: "Bearer " + config.Pterodactyl.apikeyclient,
      "Content-Type": "application/json",
      Accept: "Application/vnd.pterodactyl.v1+json"
    }
  })
    .then(response => {
      nodeStatus.set("node1", {
        status: "Online <:online:771545087288541184>"
      });
    })
    .catch(error => {
      nodeStatus.set("node1", {
        status: "Offline <:offline:771545087259443210>"
      });
    });

  axios({
    url: config.Pterodactyl.hosturl + "/api/client/servers/352fb41b/resources",
    method: "GET",
    followRedirect: true,
    maxRedirects: 5,
    headers: {
      Authorization: "Bearer " + config.Pterodactyl.apikeyclient,
      "Content-Type": "application/json",
      Accept: "Application/vnd.pterodactyl.v1+json"
    }
  })
    .then(response => {
      nodeStatus.set("node2", {
        status: "Online <:online:771545087288541184>"
      });
    })
    .catch(error => {
      nodeStatus.set("node2", {
        status: "Offline <:offline:771545087259443210>"
      });
    });

  var hosts = ["http://161.97.126.45", "https://cdn.astrobot.org"];
  hosts.forEach(function(host) {
    axios({
      url: host,
      method: "GET"
    })
      .then(response => {
        nodeStatus.set(host, {
          status: "Online <:online:771545087288541184>"
        });
      })
      .catch(error => {
        nodeStatus.set(host, {
          status: "Offline <:offline:771545087259443210>"
        });
      });
  });
};
