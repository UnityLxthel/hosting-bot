const axios = require("axios");
const Discord = require("discord.js");

async function channels(Astro) {
  console.log("Astronomy Hosting (Bot) Editing Channels.");

  let guilds = Astro.guilds.cache.get("770348084395638795"); // guild
  let channel = Astro.channels.cache.get("770348785972805673"); //member count channel
  let users = Number(guilds.memberCount);
  channel.edit({ name: `Members: ${users.toLocaleString()}` });

  let channel3 = Astro.channels.cache.get("770415040628129792"); // boost count channel
  channel3.edit({ name: `Boosts: ${guilds.premiumSubscriptionCount}` });
    
  let channel2 = Astro.channels.cache.get("773621244258942986"); // ticket count channel
  channel2.edit({ name: `Tickets: ${guilds.channels.cache.filter(x => x.name.endsWith("-ticket")).size}` });

  axios({
    url: config.Pterodactyl.hosturl + "/api/application/servers",
    method: "GET",
    followRedirect: true,
    maxRedirects: 5,
    headers: {
      Authorization: "Bearer " + config.Pterodactyl.apikey,
      "Content-Type": "application/json",
      Accept: "Application/vnd.pterodactyl.v1+json"
    }
  }).then(response => {
    Astro.channels.cache.get("770348915655573506").edit({
      name: `Servers Hosting: ${response.data.meta.pagination.total}`,
      reason: "Server count update"
    });
  });
}

module.exports = channels;