// finds out how many servers a user has

const axios = require("axios");

module.exports = async Mythical => {
  console.log("Astronomy Hosting (Bot) Fetching user information.");

  try {
    let all = []; // array of every server on the host

    axios({
      url: config.Pterodactyl.hosturl + "/api/application/servers",
      method: "GET",
      followRedirect: true,
      maxRedirects: 5,
      headers: {
        Authorization: "Bearer " + config.Pterodactyl.apikey,
        "Content-Type": "application/json",
        Accept: "Application/vnd.pterodactyl.v1+json"
      }
    }).then(resources => {
      var countmax = resources.data.meta.pagination.total_pages;
      var i2 = countmax++;

      var i = 0;
      while (i < i2) {
        //console.log(i)
        axios({
          url:
            config.Pterodactyl.hosturl + "/api/application/servers?page=" + i,
          method: "GET",
          followRedirect: true,
          maxRedirects: 5,
          headers: {
            Authorization: "Bearer " + config.Pterodactyl.apikey,
            "Content-Type": "application/json",
            Accept: "Application/vnd.pterodactyl.v1+json"
          }
        }).then(response => {
          //console.log(resources.data.meta)
          all.push(...response.data.data);
        });
        i++;
      }
      //console.log(resources.data.meta.pagination);
      var total = resources.data.meta.pagination.total;
    });

    setTimeout(async () => {
      let Guild = Mythical.guilds.cache.get("770348084395638795");

      Guild.members.cache.map(async member => {
        let ID = member.user.id;
        let userAllowed = Mythical.userAllowed.get(ID);
        let i = userData.get(ID);

        if (i) {
          const output = await all.filter(usr =>
            usr.attributes
              ? usr.attributes.user == userData.get(ID).consoleID
              : false
          );
          //console.log(output.length);

          if (userAllowed) {
            let data = {
              id: ID,
              desc: userAllowed.desc,
              allowed: userAllowed.allowed,
              has: output.length,
              payments: userAllowed.payments
            };

            Mythical.userAllowed.set(ID, data);
          } else {
            let data = {
              id: ID,
              desc: null,
              allowed: 1,
              has: output.length,
              payments: []
            };

            Mythical.userAllowed.set(ID, data);
          }
        } // no user account
      });
    }, 10000);
      
      // get user information
      /*
      let allusers = [];
      let Guild = Mythical.guilds.cache.get("770348084395638795");

      Guild.members.cache.map(async member => {
          allusers.push(member.user.id);
      });
      
      setTimeout(async () => {
      allusers.chunk_inefficient(3)
      }, 10000); */
      
  } catch (e) {
    console.log(`[Jobs Error (user) ] - Error: \n ${e}`);
    Mythical.channels.cache
      .get("770352440788647955")
      .send(`[Jobs Error ( user ) ] - Error: \n ${e}`);
  }
};

Object.defineProperty(Array.prototype, 'chunk_inefficient', {
  value: function(chunkSize) {
    var array = this;
    return [].concat.apply([],
      array.map(function(elem, i) {
        return i % chunkSize ? [] : [array.slice(i, i + chunkSize)];
      })
    );
  }
});