const Enmap = require("enmap");
const cron = require("node-cron");
const Discord = require("discord.js");

module.exports = async Astro => {
  console.log("( Bot ) Connected to Discord.");

  cron.schedule("*/5 * * * *", function() {
    // edits channels every 5mins
    require(process.cwd() + "/src/Bot/Jobs/channels.js")(Astro);
  });

  cron.schedule("* * * * *", function() {
    // gets user data every minute
    require(process.cwd() + "/src/Bot/Jobs/user.js")(Astro);
  });
    
  cron.schedule("* * * * *", function() {
    // gets a nodes information every minute
    require(process.cwd() + "/src/Bot/Jobs/nodes.js")(Astro);
  });

  require(process.cwd() + "/src/Bot/Jobs/nodes.js")(Astro);

  //Check make sure create account channels are closed after a hour
  setTimeout(() => {
    Astro.guilds.cache
      .get("770348084395638795")
      .channels.cache.filter(
        x =>
          x.parentID == "770430764630016040" &&
          Date.now() - x.createdAt > 1800000
      )
      .forEach(x => x.delete());
  }, 60000);

  Astro.user.setActivity(`Over The Host`, {
    type: "WATCHING"
  });
    
   let guild = Astro.guilds.cache.get("770348084395638795");
  // Save the current collection of guild invites.
  guild.fetchInvites().then(guildInvites => {
    Astro.invites = guildInvites;
  });

  //Dashboard Owner Sync
  Astro.appInfo = await Astro.fetchApplication();
  setInterval(async () => {
    Astro.appInfo = await Astro.fetchApplication();
  }, 60000);

  Object.assign(
    Astro,
    Enmap.multi(
      [
        "enabledCmds",
        "userDB",
        "emojiDB",
        "villagerDB",
        "tags",
        "playlist",
        "infractionDB",
        "sessionDB",
        "muteDB",
        "memberStats",
        "reactionRoleDB"
      ],
      { ensureProps: true }
    )
  );

  Astro.userAllowed = new Enmap({ name: "users" });
  Astro.settings = new Enmap({ name: "user-settings" });


  // Emoji usage tracking database init
  guild.emojis.cache.forEach(e => {
    // If EmojiDB does not have the emoji, add it.
    if (!Astro.emojiDB.has(e.id)) {
      Astro.emojiDB.set(e.id, 0);
    }
  });
  // Sweep emojis from the DB that are no longer in the guild emojis
  Astro.emojiDB.sweep((v, k) => !guild.emojis.cache.has(k));

  //require("../RoleReaction.js")(Bumper);
};
