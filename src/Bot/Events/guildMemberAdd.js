const Discord = require("discord.js");

module.exports = async (Bumper, member) => {
  // GuildMemberAdd Event.

  let guild = Bumper.guilds.cache.get("770348084395638795");
    
  let normal = guild.roles.cache.get("770354850093072414");
  let blank = guild.roles.cache.get("770406933952987147");
  let bots = guild.roles.cache.get("770355550377738310");

  if (member.user.bot) {
    member.roles
      .add(bots, "Auto assigned role / bot")
      .catch(e => console.log(e)); // Adds the blank role to the user.
  } else {
    let Roles = [];
    Roles.push(blank);
    Roles.push(normal);
    member.roles.add(Roles, "Auto assigned role").catch(e => console.log(e)); // Adds the blank role to the user.
    addInv(Bumper, member);
  }

  let channel = Bumper.channels.cache.get("770353444597923840"); // welcome

  let msg = `Welcome ${member.user} To Astrology Bot Hosting, do us a favor and get your self some roles in <#770353388935315478> and read our rules  <#770348408685854760>`;

 

  channel.send(`${msg}`);
  channel.setTopic(
    `${member.guild.name} Now has #${guild.memberCount} members`
  );
};

async function addInv(client, member) {
  const time = Date.now();
  let accountAge = client.humanTimeBetween(time, member.user.createdTimestamp);

  // 172,800,000 ms is 48 hours.
  if (time - member.user.createdTimestamp < 172800000) {
    accountAge = `${client.emoji.warning} **NEW ACCOUNT** ${client.emoji.warning} ${accountAge}`;
  }

  let inviteField = "Unknown";
  // Check which invite was used.
  const guildInvites = await member.guild.fetchInvites();
  // Existing invites
  const ei = client.invites;
  // Update cached invites
  client.invites = guildInvites;
  // Discover which invite was used
  const invite = guildInvites.find(i => {
    if (!ei.has(i.code)) {
      // This is a new code, check if it's used.
      return i.uses > 0;
    }
    // This is a cached code, check if it's uses increased.
    return ei.get(i.code).uses < i.uses;
  });
  // If invite isn't valid, that most likely means the vanity URL was used so default to it.
  if (invite) {
    // Inviter
    const inviter = client.users.cache.get(invite.inviter.id);
    inviteField = `${invite.code} from ${inviter.tag} (${inviter.id}) with ${invite.uses}`;
  } else {
    // Vanity URL was used
    inviteField = "Vanity URL";
  }

  const embed = new Discord.MessageEmbed()
    .setTitle("User Joined")
    .setColor(client.Color)
    .setThumbnail(
      "https://cdn2.iconfinder.com/data/icons/circle-icons-1/64/mail-128.png"
    )
    .setDescription(`**${member.user.tag}**\`(${member.user.id})\` has joined!`)
    // .addField('**Member Joined**', `<@${member.id}>`, true)
    .addField("**Join Position**", member.guild.memberCount, true)
    .addField("**Account Age**", accountAge, true)
    .addField("**Invite Used**", inviteField, true);

  client.channels.cache.get("770352413621354547").send(embed); // user-log
}