const zodiac = require("zodiac-signs")();
console.log("[Astronomy] (Bot) Zodiac Client is starting!");
//console.log(sign.element)

module.exports = async function ( Mythical )  {
  let sign = zodiac.getSignByDate();
  console.log(sign);
  Mythical.Zodiac = sign;
  Mythical.Element = sign.element;

  if (sign.name === "Aries") {
    Mythical.Color = "#FF553D";
    Mythical.Image =
      "https://astrobot.org/Images/aries.png";
  }

  if (sign.name === "Taurus") {
    Mythical.Color = "#ffd1dc";
    Mythical.Image =
      "https://astrobot.org/Images/taurus.png";
  }

  if (sign.name === "Gemini") {
    Mythical.Color = "#ffff00";
    Mythical.Image =
      "https://astrobot.org/Images/gemini.png";
  }

  if (sign.name === "Cancer") {
    Mythical.Color = "#c4d4e0";
    Mythical.Image = "https://astrobot.org/Images/cancer.png";
  }

  if (sign.name === "Leo") {
    Mythical.Color = "#f9d71c";
    Mythical.Image = "https://astrobot.org/Images/leo.png";
  }

  if (sign.name === "Virgo") {
    Mythical.Color = "#266F00";
    Mythical.Image = "https://astrobot.org/images/virgo.png";
  }

  if (sign.name === "Libra") {
    Mythical.Color = "#98fb98";
    Mythical.Image =
      "https://astrobot.org/Images/libra.png";
  }

  if (sign.name === "Scorpio") {
    Mythical.Color = "#800000";
    Mythical.Image = "https://astrobot.org/Images/scorpio.png";
  }

  if (sign.name === "Sagittarius") {
    Mythical.Color = "#720058";
    Mythical.Image =
      "https://astrobot.org/Images/sagittarius.png";
  }

  if (sign.name === "Capricorn") {
    Mythical.Color = "#90ee90";
    Mythical.Image =
      "https://astrobot.org/Images/capricorn.png";
  }

  if (sign.name === "Aquarius") {
    Mythical.Color = "#40e0d0";
    Mythical.Image =
      "https://astrobot.org/images/aquarius.png";
  }

  if (sign.name === "Pisces") {
    Mythical.Color = "#93E9BE";
    Mythical.Image =
      "https://astrobot.org/Images/pisces.jpg";
  }
  
  // pride logos
  let gemini = "https://astrobot.org/Images/pride.gemini.png";
  let cancer = "https://astrobot.org/Images/pride.cancer.png";
  let leo = "https://astrobot.org/Images/pride.leo.png";
  let virgo = "https://astrobot.org/Images/pride.virgo.png";
  let scorpio = "https://astrobot.org/Images/pride.scorpio.png";
  
  Mythical.Image = scorpio; // for pride
    
}