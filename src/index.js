// Imports
const Discord = require("discord.js");
const { Client, Collection } = require("discord.js");
let settings = require("../settings.json");
global.config = require("../settings.json");
global.settings = require("../settings.json");
const manager = new Discord.WebhookClient(
  config.botlogin.id,
  config.botlogin.token
);
const bot = require("./Bot/AstronomyClient.js");

require("./ZodiacClient")(bot);
require("./Web/index.js")
//require("./Stats/index.js")

//Ready
bot.on("ready", m => {
  let loggedin = new Discord.MessageEmbed()
    .setTitle("Bot Running Successfully")
    .setColor(bot.Color)
    .setThumbnail(`https://ss.kyle8973.tk/lNEF`)
    .setDescription(
      `Logged In As ${bot.user.tag}! No Major Errors Occured Upon Starting`
    )
    .setTimestamp();
  console.log(`Logged In As ${bot.user.tag}`);
  manager.send(loggedin);
});

const nodemailer = require("nodemailer");
global.transport = nodemailer.createTransport({
  host: settings.Email.Host,
  port: settings.Email.Port,
  auth: {
    user: settings.Email.User,
    pass: settings.Email.Password
  }
});

let db = require("quick.db");

global.userData = new db.table("userData");
global.settings = new db.table("settings");
global.webSettings = new db.table("webSettings");
global.mutesData = new db.table("muteData");
global.moment = require("moment");
global.nodeStatus = new db.table("nodeStatus");
global.nodeData = new db.table("nodeData");

bot.login(settings.TOKEN);
