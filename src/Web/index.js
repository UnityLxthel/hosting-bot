// express server
var PORT = config.Port;
const express = require("express");
const app = express();
var Server = require("http").createServer(app);

const passport = require("passport");
const session = require("express-session");
const strategy = require("passport-discord").Strategy;
const MongoStore = require("connect-mongo")(session);

const helmet = require("helmet");
const cookieParser = require("cookie-parser");
const csrf = require("csurf");
var minifyHTML = require("express-minify-html");
const JsSearch = require("js-search");
const bodyParser = require("body-parser");
const path = require("path");

console.log("Astronomy Development (Web) Site is starting...");

const Astro = require("../Bot/AstronomyClient.js");

// routes
const MainRoute = require("./Routes/Main.js");
const APIRoute = require("./Routes/API.js");
const MeRoute = require("./Routes/Me.js");

app.set("views", "src/Web/Views");
app.use(express.static(process.cwd() + "/src/Web/Public/CSS"));
app.use(express.static(process.cwd() + "/src/Web/Public"));
app.use(express.static(process.cwd() + "/src/Web/Public/JS"));
app.engine("html", require("ejs").renderFile);

/* Login functions */
passport.serializeUser((user, done) => {
  done(null, user);
});
passport.deserializeUser((obj, done) => {
  done(null, obj);
});

passport.use(
  new strategy(
    {
      clientID: config.site.id,
      clientSecret: config.site.secret,
      callbackURL: "https://host.astrobot.org/api/callback",
      scope: ["identify", "email", "guilds.join"]
    },
    (accessToken, refreshToken, profile, done) => {
      process.nextTick(() => {
        return done(null, profile);
      });
    }
  )
);

app.use(
  session({
    store: new MongoStore({
      url: config.mongo
    }),
    secret: "FROPT",
    resave: false,
    saveUninitialized: false
  })
);

app.use(passport.initialize());
app.use(passport.session());
app.use(helmet({ frameguard: false }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());
// Put here so that it doesnt get editied by the HTML shortener
app.use("/api", APIRoute);
app.use(require("express-minify")());
app.use(
  minifyHTML({
    htmlMinifier: {
      removeComments: true,
      collapseWhitespace: true,
      collapseBooleanAttributes: true,
      removeAttributeQuotes: true,
      removeEmptyAttributes: true,
      minifyJS: true
    }
  })
);

/**
 * Routes
 */
app.use("/", MainRoute);
app.use("/me", MeRoute);

app.get("/robots.txt", (req, res) => {
  res.type("text/plain");
  res.send("User-agent: *\nDisallow:");
});

app.set("json spaces", 4);

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "/Views/"));

app.use("/images", express.static(process.cwd() + "/src/Web/Images/"));
app.use("/css", express.static(process.cwd() + "/src/Web/Public/CSS/"));

app.use("/css/", express.static(process.cwd() + "/src/Public/CSS/"));
app.use("/js", express.static(process.cwd() + "/src/Public/JS/"));

/*app.get('/css/:ID', function(req, res) {
  res.sendFile(process.cwd() + "/src/Web/Public/css/" + req.params.ID);
});
*/

/* Get page status codes */
app.use(function(err, req, res, next) {
  // treat as 404
  if (
    err.message &&
    (~err.message.indexOf("not found") ||
      ~err.message.indexOf("Cast to ObjectId failed"))
  ) {
    return next();
  }

  console.error(err.stack);

  if (err.stack.includes("ValidationError")) {
    res.status(422).render("error.ejs", {
      user: req.isAuthenticated() ? req.user : null,
      Website: "/login",
      message: "Validatoin Error",
      Astro
    });
    return;
  }

  // error page
  res.status(500).render("error.ejs", {
    user: req.isAuthenticated() ? req.user : null,
    Website: req.originalUrl,
    status: "500",
    message: err.message,
    Astro
  });
});

// assume 404 since no middleware responded
app.use(function(req, res) {
  const payload = {
    url: req.originalUrl,
    main: `/`,
    site: `Astronomy Dev`,
    error: "Not found"
  };
  res.status(404).render("error.ejs", {
    Astro,
    user: req.isAuthenticated() ? req.user : null,
    issue: payload.error,
    Website: payload.main,
    message: "Page Not Found"
  });
});


/**
 * Let our application listen to a specific port
 */
const Listener = Server.listen(1028, () => {
  console.log(
    `[ Website ] Application is listening on port: ${Listener.address().port}.`
  );
});