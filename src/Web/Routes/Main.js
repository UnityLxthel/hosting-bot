// example.com/*

const Router = require("express").Router();
const Astro = require(process.cwd() + "/src/Bot/AstronomyClient.js");
const Discord = require("discord.js");
//const sitemap = require("sitemap");

Router.get("/", async (req, res) => {
  let Page = "Home";
  let Message;
  let MessageDefined;

  // Ques
  let Query = req.query.q;
  let from = req.query.utm_source;

  if (from === "BotBlock") {
    console.log("[Website] User from Bot Block");
  }

  if (Query === "SUCCESSFULY_LOGGEDOUT") {
    (Message = "You are now logged out."), (MessageDefined = 1);
  }

  if (Query === "SENT_FEEDBACK") {
    (Message = "Your FeedBack was submitted!"), (MessageDefined = 1);
  }

  res.render("index.ejs", {
    user: req.isAuthenticated() ? req.user : null,
    Astro,
    Page,
    Message,
    MessageDefined,
    path: req.path
  });
});

Router.get("/login", (req, res) => {
  let redirect = req.query.redirect;
  if (!redirect) redirect = "/me";
  //console.log(redirect)
  res.redirect(
    "https://discord.com/oauth2/authorize?client_id=770353860572610604&redirect_uri=https%3A%2F%2Fhost.astrobot.org%2Fapi%2Fcallback&response_type=code&scope=identify%20email%20guilds.join&prompt=none&state=" +
      redirect
  );
});

Router.get("/logout", function(req, res) {
  req.session.destroy(() => {
    req.logout();
    res.redirect("/");
  });
});

Router.get("/discord", (req, res) => {
  res.redirect("https://discord.gg/duAzNKm");
});

Router.get("/data", function(req, res) {
  var ip =
    req.headers["x-forwarded-for"] ||
    req.connection.remoteAddress ||
    req.socket.remoteAddress ||
    (req.connection.socket ? req.connection.socket.remoteAddress : null);
  if (ip.substr(0, 7) == "::ffff:") {
    ip = ip.substr(7);
  }
  // console.log(ip)
  //  console.log(req.query)
  let nodes = ["161.97.126.45", "95.111.250.152", "172.18.0.1"];
  if (req.query.servername == undefined) {
    if (!nodes.includes(ip)) {
      res.redirect("/");
    } else {
      nodeData.set(req.query.speedname + "-speedtest", {
        speedname: req.query.speedname,
        ping: req.query.ping,
        download: req.query.download,
        upload: req.query.upload,
        updatetime: req.query.updatetime
      });
    }
  } else {
    if (!nodes.includes(ip)) {
      res.redirect("/");
    } else {
      nodeData.set(req.query.servername, {
        servername: req.query.servername,
        cpu: req.query.cpu,
        cpuload: req.query.cpuload,
        cputhreads: req.query.cputhreads,
        cpucores: req.query.cpucores,
        memused: req.query.memused,
        memtotal: req.query.memtotal,
        swapused: req.query.swapused,
        swaptotal: req.query.swaptotal,
        diskused: req.query.diskused,
        disktotal: req.query.disktotal,
        netrx: req.query.netrx,
        nettx: req.query.nettx,
        osplatform: req.query.osplatform,
        oslogofile: req.query.oslogofile,
        osrelease: req.query.osrelease,
        osuptime: req.query.osuptime,
        biosvendor: req.query.biosvendor,
        biosversion: req.query.biosversion,
        biosdate: req.query.biosdate,
        servermonitorversion: req.query.servermonitorversion,
        datatime: req.query.datatime,
        dockercontainers: req.query.dockercontainers,
        dockercontainersrunning: req.query.dockercontainersrunning,
        dockercontainerspaused: req.query.dockercontainerspaused,
        dockercontainersstopped: req.query.dockercontainersstopped,
        updatetime: req.query.updatetime
      });
    }
  }
});

Router.get("/user/:ID/avatar", async (req, res) => {
  let ID = req.params.ID;
  if (!ID) return res.send({ error: "Please give a bot ID" });

  if (isNaN(ID)) {
    return res.send({ error: "'id' must be a snowflake" });
  }

  Astro.users
    .fetch(ID)
    .then(async Bot => {
      res.redirect(
        `https://cdn.discordapp.com/avatars/${Bot.id}/${Bot.avatar}`
      );

      return;
    })
    .catch(console.error);
});

module.exports = Router;

/*
 * Authorization check, if not authorized return them to the login page.
 */
function checkAuth(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    req.session.backURL = req.url;

    res.redirect("/login?redirect=/me");
  }
}
