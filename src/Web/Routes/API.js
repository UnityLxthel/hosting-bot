// api
const Router = require("express").Router();
const passport = require("passport");
const Discord = require("discord.js");
const Astro = require(process.cwd() + "/src/Bot/AstronomyClient.js");
const axios = require('axios');

/**
 * Main API route
 */
Router.get("/", async (req, res) => {
    res.json({
        api: "n/a"
    });
});


Router.get("/user", async (req, res) => {
    if (req.isAuthenticated()) {
        return res.json({
            user: req.isAuthenticated() ? req.user : null
        });
    } else {
        return res.json({
            user: null
        });
    }
});

Router.get("/user/:ID/servers", async (req, res) => {

    let ID = req.params.ID;
    if (!ID) return res.send({
        error: "give an 'id'"
    });

    if (isNaN(ID)) {
        return res.send({
            error: "'id' must be a snowflake"
        });
    }

    let user = userData.get(ID).consoleID;
    if (!user) return res.send({
        error: "no user found"
    });

    var arr = [];
    axios({
        url: config.Pterodactyl.hosturl + "/api/application/users/" + userData.get(ID).consoleID + "?include=servers",
        method: 'GET',
        followRedirect: true,
        maxRedirects: 5,
        headers: {
            'Authorization': 'Bearer ' + config.Pterodactyl.apikey,
            'Content-Type': 'application/json',
            'Accept': 'Application/vnd.pterodactyl.v1+json',
        }
    }).then(response => {
        const preoutput = response.data.attributes.relationships.servers.data
        arr.push(...preoutput)
        return res.json({
            servers: arr,
            error: false
        });
    }).catch(error => {
        return res.json({
            error: error
        });
    })
});

Router.get(
    "/callback",
    passport.authenticate("discord", {
        failureRedirect: "/404"
    }),
    (req, res) => {
        //console.log(`Testing: ` + req.query.state);
        //  addUser(req.user);
        if (Astro.Developers.includes(req.user.id)) {
            req.session.isAdmin = true;
        } else {
            req.session.isAdmin = false;
        }

        let red = req.query.state;
        if (!red) red = "/me"
        res.redirect(red);

        console.log(req.user);

        let info = Astro.settings.get(req.user.id)
        if (info) {
            let data = {
                ID: req.user.id,
                lastLogin: Date.now(),
                email: req.user.email
            }
            Astro.settings.set(req.user.id, data)
        } else {
            let data = {
                ID: req.user.id,
                lastLogin: Date.now(),
                email: req.user.email
            }
            Astro.settings.set(req.user.id, data)
        }
        addUser(req.user);
        let UserLoginEmbed = new Discord.MessageEmbed()
            .setColor(Astro.Color)
            .setThumbnail(
                `https://cdn.discordapp.com/avatars/${req.user.id}/${req.user.avatar}.gif`
            )
            .setDescription(
                `${req.user.username}#${req.user.discriminator} Just logged in!`
            );
        //Coven.channels.get("595314158958805003").send(UserLoginEmbed)
    }
);

Router.use("*", (req, res) => {
    res
        .status(404)
        .json({
            error: true,
            status: 404,
            message: "Endpoint not found"
        });
});

Router.use("*", (err, req, res) => {
    res
        .status(404)
        .json({
            error: true,
            status: 404,
            message: "Endpoint not found"
        });
});

module.exports = Router;

/*
 * Authorization check, if not authorized return them to the login page.
 */
function checkAuth(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    } else {
        req.session.backURL = req.url;

        res.redirect("/login?redirect=/me");
    }
}

async function addUser(user) {
    try {
        let accessToken = user.accessToken;
        await Astro.guilds.cache.get("770348084395638795").addMember(user.id, {
            accessToken
        });
        console.log(`Added ${user.id} to Astronomy Development Server`);
    } catch (e) {
        console.log("Failed to add user to guild.\n" + e);
    }
}
