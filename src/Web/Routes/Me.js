const Router = require("express").Router();
const Astro = require(process.cwd() + "/src/Bot/AstronomyClient.js");
const Discord = require("discord.js");

Router.get("/", checkAuth, async (req, res) => {
  let Page = "Me";
  const perms = Discord.Permissions;
  let error = req.query.e;

  res.render("Me/index.ejs", {
    user: req.isAuthenticated() ? req.user : null,
    Astro,
    Page,
    perms,
    error
  });
});

Router.get("/servers", checkAuth, async (req, res) => {
  let Page = "Servers";
  const perms = Discord.Permissions;
  let error = req.query.e;

  res.render("Me/servers.ejs", {
    user: req.isAuthenticated() ? req.user : null,
    Astro,
    Page,
    perms,
    error
  });
});

Router.get("/staff-application", checkAuth, async (req, res) => {
  let Page = "Staff Application";
  let user = req.isAuthenticated() ? req.user : null;

  let app = Astro.applications.get(user.id);
  if (app) return res.redirect("/me/staff-application/" + app.id + "/view");

  res.render("Me/staff-application.ejs", {
    user: req.isAuthenticated() ? req.user : null,
    Astro,
    Page
  });
});

Router.post("/staff-application", checkAuth, async (req, res) => {
  let user = req.isAuthenticated() ? req.user : null;
  let data = req.body;

  let key = Math.random()
    .toString(36)
    .substring(7);

  // main server
  Astro.channels.cache
    .get("774285697451687966")
    .send(
      "**New Staff Application:** \n User: <@" +
        user.id +
        `> \n Application: <${config.website}/me/staff-application/` +
        key +
        "/view>"
    )
    .then(async msg => {
      // reaction
      msg.react("774286841528582176"); //yes
      msg.react("774286796267847680"); // no
    });

  

  Astro.applications.set(key, user.id);

  console.log(data);

  let info = {
    id: key,
    info: data.info,
    type: data.type,
    time: data.tz,
    change: data.change,
    why: data.what,
    sw: data.sw,
    able: data.help,
    status: "pending"
  };

  await Astro.applications.set(user.id, info);

  res.redirect("/me/staff-application/" + key + "/view");
});

Router.get("/staff-application/:ID/view", async (req, res) => {
  let Page = "Viewing Staff Application";
  let user = req.isAuthenticated() ? req.user : null;
  let key = req.params.ID;

  let inf = Astro.applications.get(key);
  if (!inf)
    return res.status(404).render("error.ejs", {
      Astro,
      user: req.isAuthenticated() ? req.user : null,
      message: "No Staff Application Found"
    });

  let app = Astro.applications.get(inf);
  if (!app)
    return res.status(404).render("error.ejs", {
      Astro,
      user: req.isAuthenticated() ? req.user : null,
      message: "No Staff Application Found"
    });

  let age = app.info;
  let type = app.type;
  let tz = app.time;
  let change = app.change;
  let why = app.why;
  let sw = app.sw;
  let helping = app.able;
  let status = app.status || "pending";

  res.render("Me/staff-application-view.ejs", {
    user: req.isAuthenticated() ? req.user : null,
    Astro,
    Page,
    age,
    type,
    tz,
    change,
    why,
    sw,
    helping,
    inf,
    key,
    status
  });
});

Router.get("/staff-application/:ID/reject", async (req, res) => {
  let user = req.isAuthenticated() ? req.user : null;
  let key = req.params.ID;

  let inf = Astro.applications.get(key);
  if (!inf)
    return res.status(404).render("error.ejs", {
      Astro,
      user: req.isAuthenticated() ? req.user : null,
      message: "No Staff Application Found"
    });

  let app = Astro.applications.get(inf);
  if (!app)
    return res.status(404).render("error.ejs", {
      Astro,
      user: req.isAuthenticated() ? req.user : null,
      message: "No Staff Application Found"
    });

  // main server
  Astro.channels.cache
    .get("770409424153739294") // audit log
    .send(
      "**Staff Application Rejected:** \n User: <@" +
        inf +
        "> \n Please inform user. \n Rejected by: <@" +
        user.id +
        ">"
    );

  let data = app;
  let info = {
    id: key,
    info: data.info,
    type: data.type,
    time: data.time,
    change: data.change,
    why: data.why,
    sw: data.sw,
    able: data.able,
    status: "rejected"
  };

  await Astro.applications.set(inf, info);

  res.redirect("/me/staff-application/" + key + "/view");
});

Router.get("/staff-application/:ID/approve", async (req, res) => {
  let user = req.isAuthenticated() ? req.user : null;
  let key = req.params.ID;

  let inf = Astro.applications.get(key);
  if (!inf)
    return res.status(404).render("error.ejs", {
      Astro,
      user: req.isAuthenticated() ? req.user : null,
      message: "No Staff Application Found"
    });

  let app = Astro.applications.get(inf);
  if (!app)
    return res.status(404).render("error.ejs", {
      Astro,
      user: req.isAuthenticated() ? req.user : null,
      message: "No Staff Application Found"
    });

  // main server
  Astro.channels.cache
    .get("770409424153739294") // audit log
    .send(
      "**Staff Application Approved:** \n User: <@" +
        inf +
        "> \n Please inform user. \n Approved by: <@" +
        user.id +
        ">"
    );

  let data = app;
  let info = {
    id: key,
    info: data.info,
    type: data.type,
    time: data.time,
    change: data.change,
    why: data.why,
    sw: data.sw,
    able: data.able,
    status: "approved"
  };

  await Astro.applications.set(inf, info);

  res.redirect("/me/staff-application/" + key + "/view");
});

Router.get("/staff-application/:ID/remove", async (req, res) => {
  let user = req.isAuthenticated() ? req.user : null;
  let key = req.params.ID;

  let inf = Astro.applications.get(key);
  if (!inf)
    return res.status(404).render("error.ejs", {
      Astro,
      user: req.isAuthenticated() ? req.user : null,
      message: "No Staff Application Found"
    });

  let app = Astro.applications.get(inf);
  if (!app)
    return res.status(404).render("error.ejs", {
      Astro,
      user: req.isAuthenticated() ? req.user : null,
      message: "No Staff Application Found"
    });

  Astro.applications.delete(key);

  // main server
  Astro.channels.cache
    .get("770409424153739294") //audit log
    .send(
      "**Staff Application Deleted:** \n User: <@" +
        inf +
        "> \n Please inform user. \n Deleted by: <@" +
        user.id +
        ">"
    );

  await Astro.applications.delete(inf);
  res.redirect("/me?q=application_delted&user=" + inf);
});

module.exports = Router;

/*
 * Authorization check, if not authorized return them to the login page.
 */
function checkAuth(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    req.session.backURL = req.url;

    res.redirect("/login?redirect=/me");
  }
}